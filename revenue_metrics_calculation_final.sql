WITH data_table AS (
    SELECT 
        t2.user_id,
    	DATE_TRUNC('month', payment_date::date) AS payment_month,
        SUM(t1.revenue_amount_usd) as total_revenues,
        SUM(t1.revenue_amount_usd) AS MRR,
        LAG(SUM(t1.revenue_amount_usd)) OVER (ORDER BY DATE_TRUNC('month', payment_date::date)) AS previous_MRR,
        SUM(CASE WHEN first_payment_month = DATE_TRUNC('month', payment_date::date) THEN t1.revenue_amount_usd END) AS New_MRR,
        ROUND(SUM(t1.revenue_amount_usd) / COUNT(DISTINCT t2.user_id), 2) AS ARPPU,
        COUNT(DISTINCT t2.user_id) AS paid_users,
        COUNT(DISTINCT CASE WHEN first_payment_month = DATE_TRUNC('month', payment_date::date) THEN t2.user_id END) AS new_paid_users,
        COUNT(DISTINCT CASE WHEN last_payment_month = DATE_TRUNC('month', payment_date::date) THEN t2.user_id END) AS churned_users,
        LAG(COUNT(DISTINCT t2.user_id)) OVER (ORDER BY DATE_TRUNC('month', payment_date::date)) AS previous_month_paid_users,
        SUM(CASE WHEN last_payment_month = DATE_TRUNC('month', payment_date::date) THEN t1.revenue_amount_usd END) AS churned_revenue,
        t2.game_name,
        t2.language,
        t2.has_older_device_model,
        t2.age,
        MAX(payment_date) - MIN(payment_date) AS lifetime_duration
    FROM (
        SELECT 
            user_id,
            MIN(DATE_TRUNC('month', payment_date::date)) AS first_payment_month,
            MAX(DATE_TRUNC('month', payment_date::date)) AS last_payment_month
        FROM 
            project.games_payments
        GROUP BY 
            user_id
    ) AS first_last_payments
    INNER JOIN 
        project.games_payments AS t1 ON first_last_payments.user_id = t1.user_id
    LEFT JOIN 
        project.games_paid_users AS t2 ON t1.user_id = t2.user_id
    GROUP BY 
        payment_month, t2.user_id, t2.game_name, t2.language, t2.has_older_device_model, t2.age
),
calc_table AS (
    SELECT
        user_id,
        game_name,
        language,
        has_older_device_model,
        age,
        payment_month,
        total_revenues,
        MRR,
        New_MRR,
        previous_MRR,
        ARPPU,
        paid_users,
        new_paid_users,
        churned_users,
        previous_month_paid_users,
        churned_revenue,
        CASE 
            WHEN previous_month_paid_users = 0 THEN NULL
            ELSE ROUND((churned_users::numeric / previous_month_paid_users) * 100, 2) 
        END AS churn_ratio,
        ROUND ((churned_revenue::numeric / previous_MRR) *100, 2) AS revenue_churn_rate,
        ROUND(AVG(lifetime_duration),2) AS LT,
        ROUND(AVG(total_revenues),2) AS LTV
    FROM 
        data_table
    GROUP BY 
        user_id, game_name, total_revenues, language, has_older_device_model, age, payment_month, MRR, New_MRR, previous_MRR, ARPPU, paid_users, new_paid_users, churned_users, previous_month_paid_users, churned_revenue
)
SELECT
    user_id,
    game_name,
    language,
    has_older_device_model,
    age,
    payment_month,
    total_revenues,
    MRR,
    New_MRR,
    previous_MRR,
    ARPPU,
    paid_users,
    new_paid_users,
    churned_users,
    previous_month_paid_users,
    churned_revenue,
    churn_ratio,
    revenue_churn_rate,
    LT,
    LTV	
FROM 
    calc_table
ORDER BY 
    user_id, payment_month;
   
   /* new */ 
   with monthly_revenue as (
	select
		date(date_trunc('month', payment_date)) as payment_month,
		user_id,
		game_name,
		sum(revenue_amount_usd) as total_revenue
	from project.games_payments gp 
	group by 1,2,3
),
revenue_lag_lead_months as (
	select
		*,
		date(payment_month - interval '1' month) as previous_claendar_month,
		date(payment_month + interval '1' month) as next_claendar_month,
		lag(total_revenue) over(partition by user_id order by payment_month) as previous_paid_month_revenue,
		lag(payment_month) over(partition by user_id order by payment_month) as previous_paid_month,
		lead(payment_month) over(partition by user_id order by payment_month) as next_paid_month
	from monthly_revenue
),
revenue_metrics as (
	select
		payment_month,
		user_id,
		game_name,
		total_revenue,
		case 
			when previous_paid_month is null 
				then total_revenue
		end as new_mrr,
		case 
			when previous_paid_month is null 
				then user_id
		end as new_paid_users,
		case 
			when previous_paid_month = previous_claendar_month 
				then user_id
		end as previuos_paid_users,
		previous_paid_month_revenue,
		case 
			when previous_paid_month = previous_claendar_month 
				and total_revenue > previous_paid_month_revenue 
				then total_revenue - previous_paid_month_revenue
		end as expansion_revenue,
		case 
			when previous_paid_month = previous_claendar_month 
				and total_revenue < previous_paid_month_revenue 
				then total_revenue - previous_paid_month_revenue
		end as contraction_revenue,
		case 
			when previous_paid_month != previous_claendar_month 
				and previous_paid_month is not null
				then total_revenue
		end as back_from_churn_revenue,
		case 
			when next_paid_month is null 
			or next_paid_month != next_claendar_month
				then user_id
		end as churned_users,
		case 
			when next_paid_month is null 
			or next_paid_month != next_claendar_month
				then total_revenue
		end as churned_revenue,
		case 
			when next_paid_month is null 
			or next_paid_month != next_claendar_month
				then next_claendar_month
		end as churn_month
	from revenue_lag_lead_months
)
select
	rm.*,
	gpu.language,
	gpu.has_older_device_model,
	gpu.age
from revenue_metrics rm
left join project.games_paid_users gpu using(user_id)
   ;